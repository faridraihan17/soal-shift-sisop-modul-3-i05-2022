# Laporan Penjelasan Soal Shift Modul 3 
Sistem Operasi 2022 
## IUP 

#### Asisten : Restu Agung Parama
#### Anggota : 

<ul>
<li>
Farzana Afifah Razak - 5025201130 </li>
<li>Raihan Farid - 5025201141 </li>
<li>Rangga Aulia Pradana - 5025201154 </li>
</ul>



### Daftar isi
[TOC]



## Soal 1 
Penjelasan code soal nomor 1 :


1a. this function are used to download both of the files and unzip them into two different and named them with 
 music.zip and quote.zip. Unzip was done by using thread.

```
strcat(origin, newFile);
    strcpy(destFile, origin);
    strcat(origin, ".zip");

    if(child == 0){
        char *argv[] = {"unzip", "-o", origin, "-d", destFile, NULL};
            execv("/usr/bin/unzip", argv);
    }


```
Through strcat ".zip", it will do unzip both 2 folder (quote and music) in the same time by reading the folder
name that have .zip in it.
```
if(pthread_equal(id, tid[0])){
        unzip("quote");
    }
    else if(pthread_equal(id, tid[1])){
        unzip("music");
    }

```

the unzip will use thread and use TID, where we initialize array value of 0 and 1 to unzip our files.
1b was the  decoding part using base64 and put the result in one new .txt file  for each files and on the same time using thread
and each sentance was separated using newline/enter.


Firstly we have to make function where both txt file for each senteced were fixed with enter or new line
```
char text[100] = "echo "" >> ";
        strcat(text, dir);
        execl("/bin/sh", "sh", "-c", text, (char *)0);

```
after that the decoding will start to all txt files
```
void base64(char* text){
    if(child_id == 0)
        execl("/bin/sh", "sh", "-c", text, (char *)0);
}
```
lastly is to put the result to a new file of txt with quote.txt and music.txt
```
if(pthread_equal(id, tid[2])){
        for(int i=1; i<10; i++){
            char cmd[120];
            sprintf(cmd, "base64 -d /home/bob/modul3/quote/q%d.txt >> /home/bob/modul3/quote.txt", i);
            base64(cmd);
            newLine("/home/bob/modul3/quote.txt");
        }
    }
```
```
    else if(pthread_equal(id, tid[3])){
        for(int i=1; i<10; i++){
            char cmd[120];
            sprintf(cmd, "base64 -d /home/bob/modul3/music/m%d.txt >> /home/bob/modul3/music.txt", i);
            base64(cmd);
            newLine("/home/bob/modul3/music.txt");
        }
    }
```
1c. Move the two .txt files containing the decoding result onto a new folder named 'hasil'
```
 else if(child_id == 0){
        char *argv[] = {"mkdir", "-p","/home/bob/modul3/hasil", NULL};
        execv("/usr/bin/mkdir", argv);

```
The codes will moved file of quote.txt and music.txt into the 'hasil' folder
```
else if(child_id2 == 0){
            char *argv2[] = {"mv", "/home/bob/modul3/quote.txt", "/home/bob/modul3/hasil", NULL};
            execv("/usr/bin/mv", argv2);

            else if(child_id3 == 0){
                char *argv3[] = {"mv", "/home/bob/modul3/music.txt", "/home/bob/modul3/hasil", NULL};
                execv("/usr/bin/mv", argv3);
```
1d. The result folder will be zipped into hasil.zip with the password of 'mihinomenest[Name users]'.
```
void hasilZip(char* pass){
    if(child_id == 0)
        char *argv[] = {"zip", "-P", pass, "-r", "hasil.zip", "hasil", NULL};
        execv("/usr/bin/zip", argv);

```
since the zip was made it will be using password in it.
```
    char* username = "bob";
    char pass[100] = "mihinomenestbob";
    strcat(pass, username);

```






### Permasalahan soal nomor 1 :
1e. hasn't finished due to time limitation.

## Soal 2 
Penjelasan code soal nomor 2 :-

### Permasalahan soal nomor 2 :


## Soal 3 
Penjalasan code soal  nomor 3 :
```
pthread_t tid[3]
pid_t child;

int length=3;
void* playandcount(void *arg)
{
	int status;
	char *argv[] = {"wget", NULL};
	char *argv1[] = {"mkdir", NULL};
	char *argv3[] = {"unzip", NULL};
	unsigned long i=0;
	pthread_t id=pthread_self();
	int iter;
	if(pthread_equal(id,tid[0]))
	{
		child = fork();
		if (child == 0){
			char *argv[] = {"wget", "-q", "https://drive.google.com/u/0/uc?id=1ewbXlCq-sh80WafFaoPjy2aOGIVeO-4W&export=download", "-O", "hartakarun.zip", NULL};
       		execv("/usr/bin/wget", argv);
        	exit(0);
		}
		else if (child > 0){
			while((wait(&status))>0);
			child = fork();
			
			if(child == 0){
				char *argv2[] = {"mkdir", "-p", "/mnt/c/Users/asus/shift3", NULL};
				execv("/bin/mkdir", argv2);
				exit(0);
			}
			else if (child > 0){
				child = fork();
				if (child == 0){
					char* argv3[] = {"unzip","-q","-n","hartakarun.zip", "-d", "/mnt/c/Users/asus/shift3", NULL};
					execv("/usr/bin/unzip", argv3);
					exit(0);
				}
				else {
					while((wait(&status))>0);

					if ((chdir("/mnt/c/Users/asus/shift3/")) < 0)exit(EXIT_FAILURE);
				}


			}

		}
	return NULL;
}

```

Menginisialisasi array untuk menanmpung dan menjalankan 2 buah thread untuk clear layar dan thread counter


```

void *pindah(void *nama_file)
{
    char cwd[PATH_MAX];
	char nama_direktori[200];
	char hidden[100], hiddenname[100];
	char file[100], existsfile[100];
    int i;

    strcpy(hiddenname, nama_file);
	strcpy(existsfile, nama_file);
    
    char *namaa = strrchr(hiddenname, '/');
    strcpy(hidden, namaa);

    
    if (hidden[1] == '.'){
		strcpy(nama_direktori, "Hidden");
	}
        
    
  
    else if (strstr(nama_file, ".") != NULL)
    {
        strcpy(file, nama_file);
        strtok(file, ".");
        char *point = strtok(NULL, "");
        
        for (i = 0; point[i]; i++)
        	point[i] = tolower(point[i]);
        
        strcpy(nama_direktori, point);
    }
   
    else
        strcpy(nama_direktori, "Unknown");
    

   
    int tersedia = file_c_exists(existsfile);
	
    if (tersedia)		
        mkdir(nama_direktori,  755);

   
    if (getcwd(cwd, sizeof(cwd)) != NULL)
    {
        char *nama = strrchr(nama_file, '/');
        char namafile[200];

        strcpy(namafile, cwd);

        strcat(namafile, "/");
		strcat(namafile, nama_direktori);
        strcat(namafile, nama);
		
        

        
        rename(nama_file, namafile);
    }
}

```
Berguna untuk memindahkan file sesuai format folder yang telah ditentukan 


```

int file_c_exists(const char *nama_file)
{
    struct stat buffer;
    int tersedia = stat(nama_file, &buffer);
    if (tersedia != 0)
        return 0;
    else 
        return 1;
}
```
Untuk memeriksa kondisi apakah file exist atau tidak


```

void fileRekursif(char *dasarPath)
{
    char path[1000];
    struct stat buffer;
	struct dirent *dp;
    
    DIR *dir = opendir(dasarPath);
    int n = 0;

    if (!dir)
        return;

    while ((dp = readdir(dir)) != NULL)
    {
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0)
        {
            
            strcpy(path, dasarPath);

			strcat(path, "/");
			strcat(path, dp->d_name);
            
            

            if (stat(path, &buffer) == 0 && S_ISREG(buffer.st_mode))
            {
                
                pthread_t thread;
                int err = pthread_create(&thread, NULL, pindah, (void *)path);
                pthread_join(thread, NULL);
            }fileRekursif(path);
        }

            
    }
    closedir(dir);
}
```
Fungsi rekursif untuk mengkategorikan semua file 

```
int main(int argc, char *argv[])
{
	int i=0;
	int err;
	while(i<1) 
	{
		err=pthread_create(&(tid[i]),NULL,&playandcount,NULL); 
		if(err!=0)
		{
			printf("\n can't create thread : [%s]",strerror(err));
		}
		else
		{
			printf("\n create thread success\n");
		}
		i++;
	}
	pthread_join(tid[0],NULL);
	pthread_join(tid[1],NULL);
	

	char cwd[PATH_MAX];
    
    if (strcmp(argv[1], "-f") == 0)
    {
        pthread_t thread;
        int i;
       
        for (i = 2; i < argc; i++)
        {
            char printmsg[1000];
            int tersedia = file_c_exists(argv[i]);
            if (tersedia)
            {
                sprintf(printmsg, "File %d : Berhasil Dikategorikan", i - 1);
            }
            else
            {
                sprintf(printmsg, "File %d : Sad, gagal :(", i - 1);
            }
            printf("%s\n", printmsg);
            int err = pthread_create(&thread, NULL, pindah, (void *)argv[i]);
        }
       
        pthread_join(thread, NULL);
    }
    else
    {
        
        if (strcmp(argv[1], "*") == 0)
        {
            if (getcwd(cwd, sizeof(cwd)) != NULL)
                
                fileRekursif(cwd);
            
        }
    
    }
	exit(0);
	return 0;
}
```
Fungsi main untuk menjalankan setiap thread dan function untuk mengirimkan .zip file ke server

### Permasalahan soal nomor 3:
   - 
